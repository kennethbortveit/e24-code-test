import { Component } from '@angular/core';

@Component ({
	selector: 'navigation',
	styles: [`
		.nav-link {
			color: #ffffff;	
		}
		.active {
			color: #dd0000 !important;
		}
	`],
	template: `
		<ul class="nav nav-tabs nav-fill">
			<li class="nav-item">
				<a class="nav-link" routerLink="varnish" routerLinkActive="active">Varnish</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" routerLink="rss" routerLinkActive="active">RSS</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" routerLink="json" routerLinkActive="active">JSON</a>
			</li>
		</ul>
	`
})

export class NavigationComponent {

}
