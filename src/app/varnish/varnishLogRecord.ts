export class VarnishLogRecord {

	private file: string;
	private host: string;

	constructor(file: string, host: string){
		this.file = file;
		this.host = host;
		if(this.host == null){
			this.host = "";
		}
	}

	public getFile(): string {
		return this.file;
	}

	public getHost(): string {
		let hostname = "";
		if(this.host.indexOf("://") > -1) {
			hostname = this.host.split('/')[2];
		} else {
			hostname = this.host.split('/')[0];
		}
		hostname = hostname.split(':')[0];
		
		return hostname;
	}


}
