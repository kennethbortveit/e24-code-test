import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { VarnishLogRecord } from './varnishLogRecord';

@Injectable()

export class VarnishService {
	
	private http: Http;

	constructor(http: Http){
		this.http = http;	
	}
	
	public getVarnishLogRecords(): Observable<VarnishLogRecord[]>{
		 
		return this.http.get('http://localhost:3000/varnishLogFile')
		.map((response: Response)=>{

			var records: VarnishLogRecord[] = [];			
			var jsonRecords = response.json();
			
			for(let i = 0; i < jsonRecords.length; i++){
				records.push(new VarnishLogRecord(jsonRecords[i].file, jsonRecords[i].host));
			}

			return records;
		});
	}

}
