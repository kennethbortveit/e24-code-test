import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { VarnishService } from './varnish.service';
import { VarnishLogRecord } from './varnishLogRecord';

@Component ({
	selector: 'varnish',
	styles: [`
		h2 {
			color: #ffffff;
		}
		h4 {
			color: #dd0000;
		}
		.card {
			max-width: 100%;
		}
		.card-text {
			word-wrap: break-word;
		}
	`],
	template: `
		<div class="row">
			<div class="col-12">
				<h2 class="mt-5 mb-0 text-center">Varnish</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-12">
				<div class="card mt-5">
					<div class="card-block">
						<h4 class="card-title">Top 5 Hosts</h4>
						<p class="card-text" *ngFor="let record of maxHosts; let i = index;">{{i+1}}. {{record}}, antall: {{maxHostValues[i]}}</p>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-12">
				<div class="card my-5">
					<div class="card-block">
						<h4 class="card-title">Top 5 Files</h4>
						<p class="card-text" *ngFor="let record of maxFiles; let i = index;">{{i+1}}. {{record}}, antall: {{maxFileValues[i]}}</p>
					</div>
				</div>
			</div>
		</div>
	`
})

export class VarnishComponent implements OnInit {
	
	private varnishService: VarnishService;
	private varnishLogRecords: VarnishLogRecord[];
	private countedHosts: {[host: string]: number};
	private countedFiles: {[file: string]: number};
	private maxHosts: string[];
	private maxHostValues: number[];
	private maxFiles: string[];
	private maxFileValues: number[];

	constructor(varnishService: VarnishService){
		this.varnishService = varnishService;
		this.countedHosts = {};
		this.varnishLogRecords = [];
		this.maxHosts = [];
		this.maxHostValues = [];
	}

	ngOnInit(){
		this.getVarnishLogRecords();	
	}

	getVarnishLogRecords(){
		this.varnishService.getVarnishLogRecords().subscribe((records: VarnishLogRecord[])=>{
			this.varnishLogRecords = records;	
			this.getTopHosts(this.countHosts(this.varnishLogRecords), 5);
			this.getTopFiles(this.countFiles(this.varnishLogRecords), 5);
		});
	}

	private countHosts(records: VarnishLogRecord[]): {[host: string]: number} {
		var hostMap: { [host: string]: number; } = {};	
		for(let record of records){
			hostMap[record.getHost()] = 0;
		}
		for(let record of records){
			hostMap[record.getHost()] += 1;
		}
		return hostMap;
	}
	
	private countFiles(records: VarnishLogRecord[]): {[file: string]: number} {
		var fileMap: {[file: string]: number} = {};

		for(let record of records){
			fileMap[record.getFile()] = 0;	
		}
		for(let record of records){
			fileMap[record.getFile()] += 1;
		}
		return fileMap;
	}

	private getTopHosts(countedHosts: {[host: string]: number}, quantity: number){
		
		let maxHost: string = this.getFirstObjectElement(countedHosts);
		let maxValue: number = countedHosts[maxHost];
		let maxHosts: string[] = [];
		let maxValues: number[] = [];
		
		for(let i = 0; i < quantity; i++){
			for(let record in countedHosts){	
				if(countedHosts[record] >= countedHosts[maxHost]){
					maxHost = record;
					maxValue = countedHosts[record];	
				}
			}
			maxHosts.push(maxHost);
			maxValues.push(maxValue);
			countedHosts[maxHost] = -1;
		}

		this.maxHosts = maxHosts;
		this.maxHostValues = maxValues;
	}
	
	private getTopFiles(countedFiles: {[file: string]: number}, quantity: number){
		let maxFile: string = this.getFirstObjectElement(countedFiles);
		let maxValue: number = countedFiles[maxFile];
		let maxFiles: string[] = [];
		let maxValues: number[] = [];

		for(let i = 0; i < quantity; i++){
			for(let record in countedFiles){
				if(countedFiles[record] >= countedFiles[maxFile]){
					if(record.split('/').pop() != ""){
						maxFile = record;
						maxValue = countedFiles[maxFile];
					}
				}
			}
			maxFiles.push(maxFile);
			maxValues.push(maxValue);
			countedFiles[maxFile] = -1;
		}
		this.maxFiles = maxFiles;
		this.maxFileValues = maxValues;

	}

	private getFirstObjectElement(object: {[host: string]: number}){
		for(let record in object){
			return record;
		}
	}
}
