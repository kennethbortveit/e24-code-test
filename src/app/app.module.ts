import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { JsonService } from './json/json.service';
import { VarnishService } from './varnish/varnish.service';
import { RSSService } from './rss/rss.service';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { VarnishComponent } from './varnish/varnish.component';
import { RSSComponent } from './rss/rss.component';
import { FeedComponent } from './rss/feed/feed.component';
import { ItemComponent } from './rss/feed/item/item.component';
import { JsonComponent } from './json/json.component';

@NgModule({
	declarations: [
		AppComponent,
		NavigationComponent,
		VarnishComponent,
		RSSComponent,
		FeedComponent,
		ItemComponent,
		JsonComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		RouterModule.forRoot([
			{path: '', redirectTo: '/varnish', pathMatch: 'full'},
			{path: 'varnish', component: VarnishComponent},
			{path: 'rss', component: RSSComponent},
			{path: 'json', component: JsonComponent}
		], {useHash: true})
	],
	providers: [
		JsonService,
		VarnishService,
		RSSService
	],
	bootstrap: [AppComponent]
})

export class AppModule { }
