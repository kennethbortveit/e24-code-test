import { Item } from './item/item';

export class Feed {
	
	private items: Item[];
	private title: string;
	private description: string;
	private url: string;

	constructor(
		items: Item[],
		title: string,
		description: string,
		url: string
	){
		this.items = items;
		this.title = title;
		this.description = description;
		this.url = url;
	}

	public getItems(): Item[] {
		return this.items;
	}

	public getTitle(): string {
		return this.title;
	}

	public getDescription(): string {
		return this.description;
	}

	public getUrl(): string {
		return this.url;
	}
}
