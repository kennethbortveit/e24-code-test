import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Enclosure } from './enclosure';

@Component({
	selector: 'item',
	styles: [`
		.stretch {
			width: 100%;
			height: 100%;
		}

		.text-bold {
			font-weight: bold;
		}
	`],
	template: `
		<div class="card-block">
			<div class="row">
				<div class="col-12 col-md-3">
					<div *ngFor="let enclosure of enclosures">
						<img src="{{enclosure.getUrl()}}" class="img-fluid d-block mx-auto stretch" />
					</div>
				</div>
				<div class="col-12 col-md-9">
					<div class="row">
						<div class="col-12">
							<p class="card-text text-right mb-2 text-bold">{{date | date: 'HH:mm dd/MM-yyyy'}}</p>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<h5 class="card-title">{{title}}</h5>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<p class="card-text">{{this.description}}</p>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<p class="card-text"><a href="{{url}}">{{url}}</a></p>
						</div>
					</div>
				</div>
			</div>
			
					<div class="row" style="height: 100%;">
						<div class="col-12" style="height: 100%;">
							<!-- Incorrect use of CSS -->
							<p class="card-text text-right"><a href="{{link}}" target="_blank"><button class="btn btn-primary" style="background-color: #dd0000; border: #dd0000; cursor: pointer;">Gå til artikkel</button></a></p>
						</div>
					</div>
		</div>
	`
})

export class ItemComponent {
	@Input() private date: string;
	@Input() private description: string;
	@Input() private link: string;
	@Input() private title: string;
	@Input() private url: string;
	@Input() private enclosures: Enclosure[];
	@Input() private imageRegularUrl: string;

	constructor() {
		
	}

	private getDate(): string{
		return this.date;
	}
}
