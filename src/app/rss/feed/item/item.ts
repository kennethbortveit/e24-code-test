import { Enclosure } from './enclosure';

export class Item {
	private date: string;
	private description: string;
	private link: string;
	private title: string;
	private url: string;
	private enclosures: Enclosure[];
	private imageRegularUrl: string;

	constructor(
		date: string,
		description: string,
		link: string,
		title: string,
		url: string,
		enclosures: Enclosure[],
		imageRegularUrl: string
	){
		this.date = date;
		this.description = description;
		this.link = link;
		this.title = title;
		this.url = url;
		this.enclosures = enclosures;
		this.imageRegularUrl = imageRegularUrl;
	}

	public getDate(): string {
		return this.date;
	}

	public getDescription(): string {
		return this.description;
	}

	public getLink(): string {
		return this.link;
	}

	public getTitle(): string {
		return this.title;
	}

	public getUrl(): string {
		return this.url;
	}

	public getImageRegularUrl(): string {
		return this.imageRegularUrl;
	}
}
