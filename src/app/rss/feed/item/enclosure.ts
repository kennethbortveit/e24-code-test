export class Enclosure {

	private length: string;
	private type: string;
	private url: string;

	constructor(
		length: string,
		type: string,
		url: string
	) {
		this.length = length;
		this.type = type;
		this.url = url;
	}

	public getLength(): string {
		return this.length;
	}

	public getType(): string {
		return this.type;
	}

	public getUrl(): string {
		return this.url;
	}
}
