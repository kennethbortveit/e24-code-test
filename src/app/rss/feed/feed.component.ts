import { Component } from '@angular/core';
import { Item} from './item/item';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
	selector: 'feed',
	styles: [`
		.btn-background {
			background-color: #dd0000;
			border-color: #dd0000;
		}

		.cursor-pointer {
			cursor: pointer;
		}
	`],
	template: `
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-12">
						<!-- Incorrect use of CSS. -->
						<button class="btn btn-primary btn-background mt-5" style="cursor: pointer;" (click)="updateFeed()">Oppdater</button>
					</div>
				</div>
				<div class="card my-3" *ngFor="let item of getSortedItems()">
					<item
						[date] = "item.date"
						[description] = "item.description"
						[link] = "item.link"
						[title] = "item.title"
						[url] = "item.url"
						[enclosures] = "item.enclosures"
						[imageRegularUrl] = "item.imageRegularUrl"
					></item>
				</div>
			</div>
		</div>
	`
})

export class FeedComponent {
	@Input() private items: Item[];
	@Input() private title: string;
	@Input() private description: string;
	@Input() private url: string;
	@Output() private update: EventEmitter<string>;
	private timer;

	constructor(){
		this.update = new EventEmitter<string>();

		// Update the feed every 60 seconds.
		this.timer = setInterval(()=>{
			this.updateFeed();	
		}, 60000);
	}

	private getSortedItems(): Item[]{
		return this.items.sort((n1, n2)=>{
			if(n1.getDate() < n2.getDate()){
				return 1;
			} else if (n1.getDate() > n2.getDate()){
				return -1;
			} else {
				return 0;
			}
		});
	}
	
	// Calls function in parent component.
	private updateFeed(){	
		this.update.emit("");
	}
}
