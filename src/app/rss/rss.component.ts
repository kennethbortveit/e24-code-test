import { Component } from '@angular/core';
import { RSSService } from './rss.service';
import { OnInit } from '@angular/core';
import { Feed } from './feed/feed';
import { Item } from './feed/item/item';

@Component({
	selector: 'rss',
	styles: [`
		h2 {
			color: #ffffff;
		}
	`],
	template: `
		<h2 class="mt-5 text-center">RSS</h2>
		<feed
			[items]="rssFeedJson.items"	
			[title]="rssFeedJson.title"
			[description]="rssFeedJson.description"
			[url]="rssFeedJson.url"
			(update)="getRSSFeedJson()"
		></feed>
	`
})

export class RSSComponent implements OnInit {
	
	private rssFeed: string;
	private rssFeedJson: Feed;
	private rssService: RSSService;
	
	constructor(rssService: RSSService) {
		this.rssFeed = '';
		this.rssFeedJson = new Feed([], "", "", "");
		this.rssService = rssService;
	}

	ngOnInit() {
		this.getRSSFeedJson();
	}

	private getRSSFeedJson() {
		this.rssService.getRSSFeedJson()
		.subscribe((feed: Feed)=>{
			this.rssFeedJson = feed;
		});
	}
}
