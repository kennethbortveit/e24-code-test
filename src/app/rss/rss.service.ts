import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Feed } from './feed/feed';
import { Item } from './feed/item/item';
import { Enclosure } from './feed/item/enclosure';

@Injectable()

export class RSSService {
	
	private http: Http;

	constructor(http: Http) {
		this.http = http;
	}
	
	public getRSSFeedJson(): Observable<Feed> {
		return this.http.get('http://localhost:3000/rssFeed')
		.map((response: Response)=>{
			let feedJson = response.json();
			let items: Item[] = [];
			
			for(let item of feedJson){
				let enclosures: Enclosure[] = []
				for(let enclosure of item.enclosures){
					enclosures.push(new Enclosure(enclosure.length, enclosure.type, enclosure.url));
				}
				items.push(new Item(item.date, item.description, item.link, item.title, item.url, enclosures, item['rss:imgregular']['#']));		
			}
			


			return new Feed(items, feedJson.title, feedJson.description, feedJson.url);	
		});
	}
}
