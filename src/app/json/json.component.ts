import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { JsonArticle } from './json-article/json-article';
import { JsonService } from './json.service';

@Component ({
	selector: 'json',
	styles: [`
		h2 {
			color: #ffffff;
		}

		.text-bold {
			font-weight: bold;
		}

		.link-background {
			background-color: #dd0000 !important;
		}

	`],
	template: `
		<div class="row">
			<div class="col-12">
				<h2 class="my-5 text-center">JSON</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card my-3" *ngFor="let article of articles">
					<div class="card-block">
						<div class="row">
							<div class="col-12 col-lg-4">
								<p class="card-text text-lg-left text-bold">{{article.getTime()}}</p>
							</div>
							<div class="col-12 col-lg-4">
								<p class="card-text text-lg-center text-bold">{{article.getDate()}}</p>
							</div>
							<div class="col-12 col-lg-4">
								<p class="card-text text-lg-right text-bold">{{article.getCategory()}}</p>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<h4 class="card-title mt-3">{{article.getTitle()}}</h4>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<p class="card-text">{{article.getDescription()}}</p>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<!-- Incorrect use of CSS -->
								<p class="card-text text-right text-bold mt-3"><a href="{{article.getLink()}}" target="_blank"><button class="btn btn-primary" style="background-color: #dd0000; border: #dd0000; cursor: pointer;">Gå til artikkel</button></a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	`
})

export class JsonComponent implements OnInit {

	private jsonService: JsonService;
	private articles: JsonArticle[];
	
	constructor(jsonService: JsonService){
		this.jsonService = jsonService;
	}

	ngOnInit(){
		this.getArticles();
	}

	private getArticles(){
		this.jsonService.getJsonTestData().subscribe((articles: JsonArticle[])=>{
			this.articles = articles;
		});
	}
}
