import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { JsonArticle } from './json-article/json-article';
import 'rxjs/add/operator/map';

@Injectable()

export class JsonService {
	
	private http: Http;

	constructor(http: Http){
		this.http = http;
	}

	public getJsonTestData(): Observable<JsonArticle[]> {
		/*
			TODO:
			Må ordne denne slik at den leser filen fra 'http://localhost:3000/files/testfeed.json'.

		*/

		return this.http.get('http://localhost:3000/jsonTestfeed')
		.map((response: Response)=>{
			let jsonArticles: JsonArticle[]	= [];
			let jsonResponse = response.json();

			for(let i = 0; i < jsonResponse.length; i++){
				jsonArticles.push(new JsonArticle(
					jsonResponse[i].title,
					jsonResponse[i].link,
					jsonResponse[i].description,
					jsonResponse[i].date,
					jsonResponse[i].time,
					jsonResponse[i].category
				));
			}

			return this.sortJsonTestData(jsonArticles);

		});	
	}

	public sortJsonTestData(articles: JsonArticle[]): JsonArticle[]{
		/*
			TODO:
			Vil gjerne fikse på denne slik at den sammenlikner Date objekter.
		*/
		return articles.sort((n1, n2)=>{

			let n1DateTimeString: string = this.convertDateToNumberFormat(n1.getDate(), n1.getTime());
			let n2DateTimeString: string = this.convertDateToNumberFormat(n2.getDate(), n2.getTime());
			
			if(n1DateTimeString < n2DateTimeString){
				return 1;
			} else if(n1DateTimeString > n2DateTimeString) {
				return -1;
			} else{
				return 0;
			}
		});	
	}

	
	public convertDateToNumberFormat(dateString: string, time: string): string{
		let date = dateString.split(' ')[0];
		let month = this.convertMonthToNumberFormat(dateString.split(' ')[1]);
		let year = dateString.split(' ')[2];
		
		return year + '-' + month + '-' + date + 'T' + time + ':00';
		
	}

	public convertMonthToNumberFormat(month: string): string{
		
		month = month.toLowerCase();
		
		switch (month) {
			case 'januar':
				return '01';

			case 'februar':
				return '02';

			case 'mars':
				return '03';

			case 'april':
				return '04';

			case 'mai':
				return '05';

			case 'juni':
				return '06';

			case 'juli':
				return '07';

			case 'august':
				return '08';
			
			case 'september':
				return '09';

			case 'oktober':
				return '10';

			case 'november':
				return '11';
			
			case 'desember':
				return '12';

			default:
				return '01';
		}
	}
}
