export class JsonArticle {

	private title: string;
	private link: string;
	private description: string;
	private date: string;
	private time: string;
	private category: string;

	constructor(
		title: string, 
		link: string, 
		description: string, 
		date: string, 
		time: string, 
		category: string) {
		
		this.title = title;
		this.link = link;
		this.description = description;
		this.date = date;
		this.time = time;
		this.category = category;
	}

	public getTitle(): string {
		return this.title;
	}

	public getLink(): string {
		return this.link;
	}
	
	public getDescription(): string {
		return this.description;
	}

	public getDate(): string {
		return this.date;
	}

	public getTime(): string {
		return this.time;
	}

	public getCategory(): string {
		return this.category;
	}

}
